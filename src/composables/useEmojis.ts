import { ref } from "vue"

export const useEmojis = () => {
  const isLoading = ref(false)

  const getEmojis = async () => {
    isLoading.value = true
    const res = await fetch('https://emoji-api.com/categories/smileys-emotion?access_key=ceb78388f3b6ff50a221d2f3dae1c68b52fefb2b')
    const emojis = await res.json()
    isLoading.value = false

    return emojis
  }

  return {
    isLoading,
    getEmojis
  }
}