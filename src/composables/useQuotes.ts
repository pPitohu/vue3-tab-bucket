import { ref } from "vue"

export const useQuotes = () => {
  const isLoading = ref(false)

  const getQuotes = async () => {
    isLoading.value = true
    const res = await fetch('https://api.quotable.io/quotes/random?limit=10')
    const quotes = await res.json()
    isLoading.value = false

    return quotes
  }

  return {
    isLoading,
    getQuotes
  }
}