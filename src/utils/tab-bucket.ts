import { App } from "vue";
import TabBucket from "@/components/TabBucket.vue";

export const registerTabBucket = (app: App) =>
  app.component('tab-bucket', TabBucket)
