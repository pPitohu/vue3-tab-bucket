export interface Quote {
  content: string,
  author: string
}

export interface Emoji {
  character: string,
  unicodeName: string
}
