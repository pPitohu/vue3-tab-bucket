/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'
import './styles.scss'

// Plugins
import { registerPlugins } from '@/plugins'
import { registerTabBucket } from '@/utils/tab-bucket'

const app = createApp(App)

registerPlugins(app)
registerTabBucket(app); // there can be registerComponents([...customComponents]) if many

app.mount('#app')
